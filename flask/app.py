from flask import Flask, send_from_directory
import sqlite3
from flask_cors import CORS
import re
import os

app = Flask(__name__)
CORS(app)

def get_db():
    dbfile = os.getenv('ANKI_SQLITE3_DATABASE_FULLPATH')
    return sqlite3.connect(dbfile)

@app.route('/')
def index():
    return {'result': 'ignore'}
    
@app.route('/images/<path:filename>')
def images(filename):
    print(filename)
    return send_from_directory(os.getenv("ANKI_IMAGES_DIRECTORY"), filename=filename, as_attachment=True)

@app.route('/hello/<int:num>')
def say_hello_world(num):
    sql = get_db().cursor()
    rows = [row for row in sql.execute("SELECT * FROM notes;")]
    notes = rows[-1*num:]
    def format_note(note):
        front, back = note[6].split('\x1f')
        def image_path_change(s):
            return re.sub(r'<\s*img\s*src\s*=\s*"(.+)"\s*/>', r'<img src="images/\1" />', s)

        front, back = (image_path_change(front), image_path_change(back))
        print(front, back)
        return {'front' : front, 'back' : back}

    json_notes = {'notes': [format_note(note) for note in notes]}
    return json_notes
