# Anki Mind Map

Create a mind map from your Anki notes. Uses flask to access the Anki database. [ReactFlow](https://reactflow.dev/) renders a graph.  