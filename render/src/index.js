import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import reportWebVitals from './reportWebVitals';
import AnkiFetchNote from './AnkiFetchNote';

const graphStyles = { width: "100%", height: "700px" };

ReactDOM.render(
  <React.StrictMode>
    <div style={graphStyles}>
        <AnkiFetchNote num="10" />
    </div>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
