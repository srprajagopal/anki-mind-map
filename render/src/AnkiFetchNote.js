import React, {useState, useEffect} from 'react';
import AnkiNodeFlow from './AnkiNodeFlow';

function AnkiFetchNote (props) {
    const [notes, setNotes] = useState([]);
    const [isLoaded, setIsLoaded] = useState(false);
    const [error, setError] = useState(null);
    useEffect( () => {
        fetch('/hello/' + props.num ).then(res => res.json()).then(
            (result) => {
                setNotes(result.notes);
                setIsLoaded(true);
            },
            (error) => {
                setIsLoaded(true);
                setError(error);
            }
        )
    }, []) 

    if (error) {
        return <p> Error </p>
    }

    else if (!isLoaded) {
        return <p> Populating ... </p>
    }

    else {
        const initialElements = notes.map( (note, i) => { 
                return {id: String(i + 1), type: 'ankiNode', data: { front: note.front, back: note.back }, position: { x: parseInt(200 + Math.random() * 500) , y: parseInt(200 + Math.random() * 500) }, className: "light" };
        });
        return <AnkiNodeFlow initialElements={initialElements} />
    }
}

export default AnkiFetchNote;
